package pokemonleague;

import java.lang.Object;
import java.util.Scanner;

import org.apache.commons.lang3.ArrayUtils;
/**
 *
 * @author Daniel Sarmiento y Miller Capera
 * Grupo 500
 */

/**
 * Clase PokemonLeage principal del juego.
 */
public class PokemonLeague {

    /**
     * Método main de la aplicacion.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /**
         * Objeto Scanner para capturar datos.
         */        
        Scanner teclado = new Scanner(System.in);
        
        /**
         * Método para el mensaje inicial.
         */
        menuPrincipal();
        
        /**
         * Creamos un arreglo de la clase Entrenador, para guardar todos los 
         * entrenadores del juego.
         */        
        Entrenador entrenadores[] = new Entrenador[6];
                
        /**
         * Creamos un arreglo para los pokemones del torneo digitados por el usuario.
         */
        Pokemons pokemones_torneo[] = new Pokemons[18];

        /**
         * Usamos el método de registro de entrenadores.
         */        
        registroEntrenador(entrenadores);

        /**
         * Usamos el método para mostrar los entrenadores registrados.
         */
        mostrarEntrenador(entrenadores);
                
        /**
         * Creamos los Pokemones para el juego.
         */
        crearPokemon(pokemones_torneo);
        
        /**
         * Usamos el método para mostrar los pokemones registrados.
         */
        mostrarPokemones(pokemones_torneo);
        
        /**
         * Asignación pokemon entrenadores.
         */        
        entrenadores[0].pokemones_propios[0] = pokemones_torneo[0];
        entrenadores[0].pokemones_propios[1] = pokemones_torneo[1];
        entrenadores[0].pokemones_propios[2] = pokemones_torneo[2];
        
        entrenadores[1].pokemones_propios[0] = pokemones_torneo[3];
        entrenadores[1].pokemones_propios[1] = pokemones_torneo[4];
        entrenadores[1].pokemones_propios[2] = pokemones_torneo[5];        
        
        entrenadores[2].pokemones_propios[0] = pokemones_torneo[6];
        entrenadores[2].pokemones_propios[1] = pokemones_torneo[7];
        entrenadores[2].pokemones_propios[2] = pokemones_torneo[8];
        
        entrenadores[3].pokemones_propios[0] = pokemones_torneo[9];
        entrenadores[3].pokemones_propios[1] = pokemones_torneo[10];
        entrenadores[3].pokemones_propios[2] = pokemones_torneo[11];
        
        entrenadores[4].pokemones_propios[0] = pokemones_torneo[12];
        entrenadores[4].pokemones_propios[1] = pokemones_torneo[13];
        entrenadores[4].pokemones_propios[2] = pokemones_torneo[14];
        
        entrenadores[5].pokemones_propios[0] = pokemones_torneo[15];
        entrenadores[5].pokemones_propios[1] = pokemones_torneo[16];
        entrenadores[5].pokemones_propios[2] = pokemones_torneo[17];
        
        /**
         * Lista de entrenadores y pokemones asignados.
         */  
        System.out.println("\nSe asignaron los pokemones a sus respectivos entrenadores:");
        
        System.out.println("\nTu:");
        System.out.println("Nombre del entrenador: "+entrenadores[0].getNombre()+" / Pokemon asignado: "+entrenadores[0].pokemones_propios[0].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[0].getNombre()+" / Pokemon asignado: "+entrenadores[0].pokemones_propios[1].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[0].getNombre()+" / Pokemon asignado: "+entrenadores[0].pokemones_propios[2].getNombre());
        
        System.out.println("\nRetador 1:");
        System.out.println("Nombre del entrenador: "+entrenadores[1].getNombre()+" / Pokemon asignado: "+entrenadores[1].pokemones_propios[0].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[1].getNombre()+" / Pokemon asignado: "+entrenadores[1].pokemones_propios[1].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[1].getNombre()+" / Pokemon asignado: "+entrenadores[1].pokemones_propios[2].getNombre());
        
        System.out.println("\nRetador 2:");
        System.out.println("Nombre del entrenador: "+entrenadores[2].getNombre()+" / Pokemon asignado: "+entrenadores[2].pokemones_propios[0].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[2].getNombre()+" / Pokemon asignado: "+entrenadores[2].pokemones_propios[1].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[2].getNombre()+" / Pokemon asignado: "+entrenadores[2].pokemones_propios[2].getNombre());
        
        System.out.println("\nRetador 3:");        
        System.out.println("Nombre del entrenador: "+entrenadores[3].getNombre()+" / Pokemon asignado: "+entrenadores[3].pokemones_propios[0].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[3].getNombre()+" / Pokemon asignado: "+entrenadores[3].pokemones_propios[1].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[3].getNombre()+" / Pokemon asignado: "+entrenadores[3].pokemones_propios[2].getNombre());
        
        System.out.println("\nRetador 4:");
        System.out.println("Nombre del entrenador: "+entrenadores[4].getNombre()+" / Pokemon asignado: "+entrenadores[4].pokemones_propios[0].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[4].getNombre()+" / Pokemon asignado: "+entrenadores[4].pokemones_propios[1].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[4].getNombre()+" / Pokemon asignado: "+entrenadores[4].pokemones_propios[2].getNombre());
        
        System.out.println("\nRetador 5:");
        System.out.println("Nombre del entrenador: "+entrenadores[5].getNombre()+" / Pokemon asignado: "+entrenadores[5].pokemones_propios[0].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[5].getNombre()+" / Pokemon asignado: "+entrenadores[5].pokemones_propios[1].getNombre());
        System.out.println("Nombre del entrenador: "+entrenadores[5].getNombre()+" / Pokemon asignado: "+entrenadores[5].pokemones_propios[2].getNombre());
        
        /**
         * Elegir pokemones para comenzar el combate.
         */
        
        System.out.println("\n♦♦♦Seleccionde pokemones del jugador para comenzar♦♦♦");
        
        for(int i=0; i < entrenadores[0].pokemones_propios.length; i++) {
        	System.out.println("\nElije uno de tus pokemons: "+ (i+1) +" "+ entrenadores[0].pokemones_propios[i].getNombre() + " Tipo: "+entrenadores[0].pokemones_propios[i].getTipo());
        }
        	
        System.out.print("Tu elección: ");
        int pokemonSelect = teclado.nextInt();
         /**
          * Variables para poder realizar el combate por medio de un ciclo
          */
        int rival=1;
        int pokemonRival=0;
        int pokemonsTu=3;
        int pokemonsRival=3;
        Pokemons[] pokTu = entrenadores[0].pokemones_propios;
        /**
         * Creo objeto de un nuevo combate, para que comience 
         * con los pokemones que el jugador eligió elijan, que se pasan 
         * por los parametros.
         */
        Enfrentamiento pelea =  new Enfrentamiento(pokTu[pokemonSelect-1],entrenadores[rival].pokemones_propios[pokemonRival]);
        
        /**
         * Se comienza el combate con un turno aleatorio entre los jugadores.
         */
        int turno = pelea.InicioLucha(entrenadores[0].getNombre(),entrenadores[1].getNombre());
        int cont = 0;
        /**
         * Se obitiene el resultado de la batalla
         */
	    int resultado = pelea.Lucha(turno);
	    /**
	     * Depues del primer combate el resto se ejecutan en un ciclo while
	     */
	    while(cont<=0) {
	        switch(resultado){
	        /**
	         * Por cada resultado de la batalla hay un caso distinto
	         */
	        case 1:
	        	pokemonsRival -= 1;
	        	/**
		         * Cuando aun le quedan pokemons al rival este siemplemente cambia de pokemon
		         */
	        	if(pokemonsRival>0) {
	        		pokemonRival += 1;
	        		pelea = new Enfrentamiento(pokTu[pokemonSelect-1],entrenadores[rival].pokemones_propios[pokemonRival]);
	        		System.out.println("\n"+entrenadores[rival].getNombre()+" Va a cambiar de pokemon a "+entrenadores[rival].pokemones_propios[pokemonRival].getNombre()+" de tipo "+entrenadores[rival].pokemones_propios[pokemonRival].getTipo());
	        		resultado = pelea.Lucha(turno);
	        	}else if(pokemonsRival<=0) {
	        		System.out.println(entrenadores[rival].getNombre()+" Ha sido derrotado");
	        		/**
			         * Ganas la liga y el ciclo es completado
			         */
	                if(rival==5) {
		        		System.out.println("Felicidades! Has ganado la liga pokemon");
		        		cont = 1;
		        		break;
	                }
	                /**
	                 * Ganar una batalla te hara pasar con el siguiente entrenador, tus pokemons se recuperan por completo
	                 */
	        		System.out.println("Ahora te tocara enfrentarte a otro entrenador");
	        		rival +=1;
	                turno = pelea.InicioLucha(entrenadores[0].getNombre(),entrenadores[rival].getNombre());
	                pokTu = entrenadores[0].pokemones_propios;
	                pokTu[0].recuperarPokemons(pokTu[0]);
	                pokTu[1].recuperarPokemons(pokTu[1]);
	                pokTu[2].recuperarPokemons(pokTu[2]);
	        		pokemonRival = 0;
	        		pokemonsRival = 3;
	        		pokemonsTu = 3;
	                for(int i=0; i < pokTu.length; i++) {
	                	System.out.println("Elije uno de tus pokemons: "+ (i+1) +" "+ pokTu[i].getNombre() + " Tipo: "+pokTu[i].getTipo());
	                }
	                pokemonSelect = teclado.nextInt();
	                pelea =  new Enfrentamiento(pokTu[pokemonSelect-1],entrenadores[rival].pokemones_propios[pokemonRival]);
	                pelea.Lucha(turno);
	                }
	        		else{
	            		System.out.println("Error pokemons");
	        		}
	        	break;
	        case 2:
	        	pokemonsTu -= 1;
	        	if(pokemonsTu>0) {
	        		/**
	        		 * Cuando tu pokemon es derrotado puede elegir otro
	        		 */
	        		pokTu = ArrayUtils.removeElement(pokTu,pokTu[pokemonSelect-1]);
	        		System.out.println("Ups! has perdido un pokemon");
	        		for(int i=0; i < pokTu.length; i++) {
	                	System.out.println("Elije uno de tus pokemons: "+ (i+1) +" "+ pokTu[i].getNombre() + " Tipo: "+pokTu[i].getTipo());
	                }
	                	 pokemonSelect = teclado.nextInt();
	                     pelea =  new Enfrentamiento(pokTu[pokemonSelect-1],entrenadores[rival].pokemones_propios[pokemonRival]);
	                     resultado = pelea.Lucha(turno);
	                	 
	        	}else if(pokemonsTu<=0) {
	        		/**
	        		 * Cuando pierdes acaba el ciclo y debes intentarlo de nuevo desde el inicio
	        		 */
	        		System.out.println("Ups!, has perdido a todos tu pokemons tendras que volver a intentarlo");
	        		cont = 1;
	        		break;
	        	}
	        	break;
	        }
        }
    
    }//Fin del metodo main.
    
    
    
    //Métodos principales del juego.
    
    public static void menuPrincipal(){        
        System.out.println("***BIENVENIDO AL JUEGO DE POKEMON***"
                + "\nDesarrollado por: Daniel Sarmiento & Miller Capera.\n");
        
    }
    
    /**
     * Método para registrar los entrenadores
     * @param arreglo tipo entrenador
     */
    public static void registroEntrenador(Entrenador[] arreglo){
        Scanner teclado = new Scanner(System.in);
        
        String reg_nombre, reg_origen;
        int reg_edad;
        
        System.out.println("♦Registrese como entrenador del juego♦");
        
        //Capturo datos en las variables temporales
            System.out.print("\nSu Nombre: ");
            reg_nombre = teclado.next();            
            System.out.print("Su Origen: ");
            reg_origen = teclado.next();            
            System.out.print("Su Edad:");
            reg_edad = teclado.nextInt();
            
            //Asigno valores capturados en los objetos del arreglo de entrenadores
            arreglo[0]= new Entrenador(reg_nombre,reg_origen,reg_edad);
            
            //Se agregan los retadores para el juego
            arreglo[1] = new Entrenador("Brock","Ciudad Plateada",21);
            arreglo[1].setExperiencia(50); 
            
            arreglo[2] = new Entrenador("Misty","Ciudad Celeste",18);
            arreglo[2].setExperiencia(100); 
            
            arreglo[3] = new Entrenador("Teniente Surge","Ciudad Carmín",20);        
            arreglo[3].setExperiencia(200);                         
            
            arreglo[4] = new Entrenador("Gary","Ciudad Verde",23);        
            arreglo[4].setExperiencia(450); 
            
            arreglo[5] = new Entrenador("Ash","Ciudad Verde",23);
            arreglo[5].setExperiencia(500); 
    }
    
    /**
     * Método para leer los entrenadores.
     * @param arreglo tipo entrenador
     */   
    public static void mostrarEntrenador(Entrenador[] arreglo){
        System.out.println("\nLos datos de los entrenadores registrados son:");
        
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println("\nEntrenador número "+i+":");
            System.out.println("Nombre entrenador -->"+arreglo[i].getNombre());
            System.out.println("Origen entrenador -->"+arreglo[i].getOrigen());
            System.out.println("Edad entrenador -->"+arreglo[i].getEdad());
            System.out.println("Experiencia entrenador -->"+arreglo[i].getExperiencia());
            
        }
    }
    
    /**
     * Método para crear los pokemones.
     * @param arreglo de los pokemones para incluir en el juego.
     */    
    public static void crearPokemon(Pokemons [] arreglo){
        Scanner teclado = new Scanner(System.in);
        
        String nombre_pokemon, tipo_pokemon;
        
        System.out.println("\n♠Registre 3 pokemones para jugar♠");
        
        for (int i = 0; i < 3; i++) {
            
            //Capturo datos en las variables temporales
            System.out.print("\nNombre del pokemon "+i+": ");
            nombre_pokemon = teclado.next();            
            System.out.print("Tipo de pokemon "+i+": ");
            System.out.print("\n• Trueno."
                    + "\n• Agua."
                    + "\n• Tierra."
                    + "\n• Viento."
                    + "\n• Fuego."
                    + "\n• Veneno.");
            System.out.print("\nNota: Si se escribe uno diferente a la lista, "
                    + "tu pokemón no tendrá habilitades");
            System.out.print("\nEscriba uno: ");
            tipo_pokemon = teclado.next(); 
            
            //Asigno valores capturados en los objetos del arreglo de entrenadores
            arreglo[i]= new Pokemons(nombre_pokemon, tipo_pokemon);
        }
        
        //Agrego pokemones para los retadores
        
        //Brock
        arreglo[3]= new Pokemons("Steelix", "Tierra");
        arreglo[4]= new Pokemons("Crobat", "Veneno");
        arreglo[5]= new Pokemons("Onix", "Tierra");
                
        //Misty
        arreglo[6]= new Pokemons("Staryu", "Agua");
        arreglo[6].levelupRival(arreglo[6].getNivel(),arreglo[6].getTipo());
        arreglo[7]= new Pokemons("Horsea", "Agua");
        arreglo[8]= new Pokemons("Gyarados", "Tierra");
        
        //Teniente Surge
        arreglo[9]= new Pokemons("Voltorb", "Trueno");
        arreglo[10]= new Pokemons("Pikachu_Avanzado", "Trueno");
        arreglo[11]= new Pokemons("Raichu", "Trueno");
        arreglo[11].levelupRival(arreglo[11].getNivel(),arreglo[11].getTipo());
        arreglo[11].levelupRival(arreglo[11].getNivel(),arreglo[11].getTipo());

        //Gary
        arreglo[12]= new Pokemons("Nidoking", "Veneno");
        arreglo[12].levelupRival(arreglo[12].getNivel(),arreglo[12].getTipo());
        arreglo[13]= new Pokemons("Arcanine", "Fuego");
        arreglo[13].levelupRival(arreglo[13].getNivel(),arreglo[13].getTipo());
        arreglo[14]= new Pokemons("Eevee", "Trueno");
        arreglo[14].levelupRival(arreglo[14].getNivel(),arreglo[14].getTipo());
        
        //Ash
        arreglo[15]= new Pokemons("Pikachu", "Trueno");
        arreglo[15].levelupRival(arreglo[15].getNivel(),arreglo[15].getTipo());
        arreglo[16]= new Pokemons("Squittle", "Agua");
        arreglo[16].levelupRival(arreglo[16].getNivel(),arreglo[16].getTipo());
        arreglo[17]= new Pokemons("Charmander", "Fuego");
        arreglo[17].levelupRival(arreglo[17].getNivel(),arreglo[17].getTipo());
        arreglo[17].levelupRival(arreglo[17].getNivel(),arreglo[17].getTipo());
        
    }
    
    /**
     * Método para ver los pokemones del torneo.
     * @param arreglo de los pokemones incluidos en el juego.
     */    
    public static void mostrarPokemones(Pokemons[] arreglo){
        System.out.println("\nLos datos de los pokemones registrados son:");
        
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println("\nPokemon número "+i+":");
            System.out.println("Nombre del pokemon -->"+arreglo[i].getNombre());
            System.out.println("Tipo del pokemon -->"+arreglo[i].getTipo());
            System.out.println("Nivel del pokemon -->"+arreglo[i].getNivel());
            System.out.println("Nivel de vida del pokemon -->"+arreglo[i].getVida());
            
        }
    }
    
}//Fin clase Pkemon League
