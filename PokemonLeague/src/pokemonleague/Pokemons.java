package pokemonleague;

/**
 * Declaro clase de habilidades para los pokemones que se 
 * manerajan por objeto.
 */
class Habilidades{
        
        /**
        * Declaro los atributos de la clase Habilidades.
        */
	int poder;
	String nombre;
	
        /**
        * Constructor para asignar poder y habilidades.
        * @param poder
        * @param nombre de la habilidad.
        */
	public Habilidades(int poder,String nombre) {
		this.poder = poder;
		this.nombre = nombre;
	}
}
/**
 * Clase para crear los Pokemones.
 */
public class Pokemons {
    
        /**
        * Atibutos de la clase Pokemons.
        */
	private int experiencia = 0;
	private int vida = 15;
	private int nivel = 1;
	private String nombre;
	private String tipo;
	private Habilidades[] habilidades = new Habilidades[3];
        
        /**
        * Constructor con parámetros de la clase Pokemons.
        * @param nombre
        * @param tipo
        */
        public Pokemons(String nombre, String tipo) {            
            this.nombre = nombre;
            this.tipo = tipo;
    		Habilidades h1= new Habilidades(0,"Bloqueada");
    		this.habilidades[0] = h1;
    		this.habilidades[1] = h1;
    		this.habilidades[2] = h1;
    		this.setHabilidades(tipo, nivel);
        }
	
        /**
        * Método para dar una habilidad dependiendo del tipo.
        * @param tipo
        * @param nivel
        */	
	public void setHabilidades(String tipo,int nivel) {
                
                /**
                * Switch case para filtrar el tipo y agregar las habilidades.
                */		
		switch(tipo){
			case "Trueno":
				/**
                                * Dependiendo del nivel, se dara una habilidad.
                                */
				if(nivel==1) {
					Habilidades h1= new Habilidades(2,"Golpe Rayo");
                    this.getHabilidades()[0] = h1;
				}else if(nivel == 2) {
					Habilidades h1= new Habilidades(3,"Rayo Directo");
                    this.getHabilidades()[1] = h1;
                    this.vida += 5;
				}else if(nivel == 3) {
					Habilidades h1= new Habilidades(4,"Impactrueno");
                    this.getHabilidades()[2] = h1; 
                    this.vida += 5;
				}else {
					System.out.println("Error de nivel");
				}
				break;
			case "Agua":
				if(nivel==1) {
					Habilidades h1= new Habilidades(2,"Bolas de agua");
					this.getHabilidades()[0] = h1;
				}else if(nivel == 2) {
					Habilidades h1= new Habilidades(3,"Chorro directo");
					this.getHabilidades()[1] = h1;
                    this.vida += 5;
				}else if(nivel == 3) {
					Habilidades h1= new Habilidades(4,"Tsunami");
					this.getHabilidades()[2] = h1;
                    this.vida += 5;
				}else {
					System.out.println("Error de nivel");
				}
				break;
			case "Tierra":
				if(nivel==1) {
					Habilidades h1= new Habilidades(2,"Golpe Fuerte");
					this.getHabilidades()[0] = h1;
				}else if(nivel == 2) {
					Habilidades h1= new Habilidades(3,"Lanzamiento de roca");
                    this.getHabilidades()[1] = h1;
                    this.vida += 5;
				}else if(nivel == 3) {
					Habilidades h1= new Habilidades(4,"Terremoto");
					this.getHabilidades()[2] = h1;
                    this.vida += 5;
				}else {
					System.out.println("Error de nivel");
				}
				break;
			case "Viento":
				if(nivel==1) {
					Habilidades h1= new Habilidades(2,"Rafaga de aire");
                                        this.getHabilidades()[0] = h1;
				}else if(nivel == 2) {
					Habilidades h1= new Habilidades(3,"Viento cortante");
					this.getHabilidades()[1] = h1;
                    this.vida += 5;
				}else if(nivel == 3) {
					Habilidades h1= new Habilidades(4,"Tornado");
                    this.getHabilidades()[2] = h1;
                    this.vida += 5;
				}else {
					System.out.println("Error de nivel");
				}
				break;
			case "Fuego":
				if(nivel==1) {
					Habilidades h1= new Habilidades(2,"Bola de fuego");
					this.getHabilidades()[0] = h1;
				}else if(nivel == 2) {
					Habilidades h1= new Habilidades(3,"Golpe de lava");
					this.getHabilidades()[1] = h1;
                    this.vida += 5;
				}else if(nivel == 3) {
					Habilidades h1= new Habilidades(4,"Infierno");
                    this.getHabilidades()[2] = h1;
                    this.vida += 5;
				}else {
					System.out.println("Error de nivel");
				}
				break;
			case "Veneno":
				if(nivel==1) {
					Habilidades h1= new Habilidades(2,"Picadura");
					this.getHabilidades()[0] = h1;
				}else if(nivel == 2) {
					Habilidades h1= new Habilidades(3,"Veneno");
					this.getHabilidades()[1] = h1;
                    this.vida += 5;
				}else if(nivel == 3) {
					Habilidades h1= new Habilidades(4,"Posión desarrollada");
					this.getHabilidades()[2] = h1; 
                    this.vida += 5;
				}else {
					System.out.println("Error de nivel");
				}
				break;
		}
	}
	
	/**
        * Método para incrementar el nivel del pokemón.
        */	
	public void levelup(int nivel,String tipo) {
		if(experiencia == 100 && nivel<=3) {
		this.setHabilidades(tipo, nivel+1);
		System.out.println("Tu pokemon a subido de nivel");
		experiencia = 0;
		this.nivel+=1;
		}else {
			System.out.println(" ");
		}
	}
	
	public void levelupRival(int nivel,String tipo) {
		this.setHabilidades(tipo, nivel+1);
		this.nivel+=1;
	}
	/**
	 * 
	 * @param recuperarPokemons el cual recupera tus pokemones depues de una batalla dependiendo del nivel 
	 */
	public void recuperarPokemons(Pokemons pokemon) {
		switch(pokemon.getNivel()) {
		case 1:
			pokemon.setVida(15);
			break;
		case 2:
			pokemon.setVida(20);

		case 3:
			pokemon.setVida(25);
		}
	}

    /**
    * Setters and Getters.
    */
        
    /**
     * @return the vida
     */
    public int getVida() {
        return vida;
    }
    
    /**
     * @param vida the vida to set
     */
    public void setVida(int vida) {
        this.vida = vida;
    }

    /**
     * @return the nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * @param nivel the nivel to set
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * @return the experiencia
     */
    public int getExperiencia() {
    	return experiencia;
    }
    /**
     * @param experiencia the experiencia to set
     */
    public void setExperiencia(int experiencia) {
    	this.experiencia = experiencia;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * @return the habilidades
     */
    public Habilidades[] getHabilidades() {
        return habilidades;
    }

    /**
     * @param habilidades the habilidades to set
     */
    public void setHabilidades(Habilidades[] habilidades) {
        this.habilidades = habilidades;
    }
		
}
