**♦♦♦♦♦IMPORTANTE♦♦♦♦♦**
***********************
**♦♦♦♦♦Instrucciones De Uso Inicial♦♦♦♦♦**

***********************
*1. Clonar el proyecto.
2. Una vez abierto en NetBeans, hacer clic derecho en la sección Librerías o Libraries > Add JAR/Folder...
3. Examinamos la carpeta donde está el proyecto, y abrimos una que se llame JAR.
4. Elegimos el archivo llamado: "commons-lang3-3.9.jar", y clic en Aceptar.
5. Ejecutamos el juego y a disfrutar. ;)*

***********************

The Pokemon League

Realizar un campeonato Pokemon.

El Campeonato consiste en 
• Se registran los maestros pokemon (Se trabaja con 6 maestross) Opción de n.
• Cada maestro tendria máximo 3 Pokemon.
• Las batallas enfrentan 1 contra 1. / El ganador Avanza.
• En la siguiente batalla, el pokemon recupera Max el 50% de la vida que perdió en la anterior batalla.

***********************
Requerimientos
***********************

• Usar POO.
• Usar GitLab.
• Al iniciar el campeonato:
 • Solicitar datos (Pokemones y Entrenadores).
• Durante cada batalla: 
 • Mostrar menú de opciones del entrenador.
• Luego de la batalla:
 • Mostrar ganador y su estado.

***********************
Extras
***********************

Puntos adicionales por generar la documentación
(diagrama clases / casos uso / usar herramientas GitLab).

***********************
Clase entrenador, pokemon.
***********************



**♦♦♦♦♦ Validación software ♦♦♦♦♦**

Se realizan los siguientes análisis:
♦	Se ingresa al proyecto asignado.
♦	Se realiza el forkeo del código al repositorio principal, en este caso SmithAlvarez.
♦	Se ingresa a Netbeans para clonar el código.
♦	Se realiza la validación parte por parte donde se ingresa la información solicitada en el documento inicial que para este caso son 6 entrenadores Pokemon pero con n cantidad de entrenadores sobre la batalla.
Se evidencia el buen manejo del uso de casos, las creaciones de las clases, constructores y demás.
Se realiza una observación de mejora para el usuario, es que al asignar una opción por parte del usuario se elimine la información que ya no es necesaria de ver, ya que esto hace que el usuario no tenga que subir la pantalla o mover la barra para leer lo nuevo generado por el código.

Con tal validación se observa que la nota establecida es: --> [4.8]
